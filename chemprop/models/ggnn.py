import torch
import torch.nn as nn

from chemprop.args import TrainArgs
from chemprop.features.featurization import BatchMolGraph, get_atom_fdim

from functools import reduce


class GGNNEncoder(nn.Module):
    """
    Gated Graph Sequence Neural Networks (GGNN)
    Mode: SelectNode
    Implementation based on https://arxiv.org/abs/1511.05493
    """
    def __init__(self, args: TrainArgs, atom_fdim: int = None):
        super(GGNNEncoder, self).__init__()

        self.hidden_dim = args.hidden_size
        self.n_node = atom_fdim
        self.device = args.device
        self.bias = args.bias or 0

        self.W_h = nn.Linear(self.n_node, self.hidden_dim, bias=self.bias)

        self.in_fc = nn.Linear(self.hidden_dim, self.hidden_dim)
        self.out_fc = nn.Linear(self.hidden_dim, self.hidden_dim)

        self.reset_gate = nn.Sequential(
            nn.Linear(self.hidden_dim*3, self.hidden_dim),
            nn.Sigmoid()
        )
        self.update_gate = nn.Sequential(
            nn.Linear(self.hidden_dim*3, self.hidden_dim),
            nn.Sigmoid()
        )
        self.transform = nn.Sequential(
            nn.Linear(self.hidden_dim*3, self.hidden_dim),
            nn.Tanh()
        )

        # Output Model
        self.out = nn.Sequential(
            nn.Linear(self.hidden_dim, self.hidden_dim),
            nn.Tanh(),
            nn.Linear(self.hidden_dim, self.hidden_dim)
        )

        self.aggregation = args.aggregation
        self.aggregation_norm = args.aggregation_norm
        # Cached zeros
        self.cached_zero_vector = nn.Parameter(torch.zeros(self.hidden_dim), requires_grad=False)


    def forward(self, mol_graph: BatchMolGraph, *args, **kwargs):
        A = mol_graph.adj.to(self.device)
        state_cur = mol_graph.f_atoms.to(self.device)

        state_cur = self.W_h(state_cur)
        state_in = self.in_fc(state_cur)
        state_out =self.out_fc(state_cur)

        a_in = torch.matmul(A, state_in)
        a_out = torch.matmul(A.T, state_out)
        a = torch.cat([a_in, a_out, state_cur], -1)

        r = self.reset_gate(a)
        z = self.update_gate(a)
        joined_input = torch.cat([a_in, a_out, r * state_cur], -1)
        h_hat = self.transform(joined_input)
        state_cur = (1 - z) * state_cur + z * h_hat

        output = self.out(state_cur)  # num_atoms x hidden size

        # Readout
        mol_vecs = []
        for _, (a_start, a_size) in enumerate(mol_graph.a_scope):
            if a_size == 0:
                mol_vecs.append(self.cached_zero_vector)
            else:
                cur_hiddens = output.narrow(0, a_start, a_size)
                mol_vec = cur_hiddens  # (num_atoms, hidden_size)
                if self.aggregation == 'mean':
                    mol_vec = mol_vec.sum(dim=0) / a_size
                elif self.aggregation == 'sum':
                    mol_vec = mol_vec.sum(dim=0)
                elif self.aggregation == 'norm':
                    mol_vec = mol_vec.sum(dim=0) / self.aggregation_norm
                mol_vecs.append(mol_vec)

        mol_vecs = torch.stack(mol_vecs, dim=0)  # (num_molecules, hidden_size)
        return mol_vecs


class GGNN(nn.Module):
    """An :class:`GGNN` is a wrapper around :class:`GGNNEncoder` which featurizes input as needed."""

    def __init__(self, args: TrainArgs, atom_fdim: int = None, **kwargs):
        """
        :param args: A :class:`~chemprop.args.TrainArgs` object containing model arguments.
        :param atom_fdim: Atom feature vector dimension.
        """
        super(GGNN, self).__init__()
        self.atom_fdim = atom_fdim or get_atom_fdim()
        self.device = args.device

        if args.mpn_shared:
            self.encoder = nn.ModuleList(
                [GGNNEncoder(args, self.atom_fdim)] * args.number_of_molecules
            )
        else:
            self.encoder = nn.ModuleList([
                GGNNEncoder(args, self.atom_fdim)
                for _ in range(args.number_of_molecules)
            ])

    def forward(self, batch, *args) -> torch.FloatTensor:
        """
        Encodes a batch of molecules.

        :param batch: A list of list of SMILES, a list of list of RDKit molecules, or a
                      list of :class:`~chemprop.features.featurization.BatchMolGraph`.
                      The outer list or BatchMolGraph is of length :code:`num_molecules` (number of datapoints in batch),
                      the inner list is of length :code:`number_of_molecules` (number of molecules per datapoint).
        :return: A PyTorch tensor of shape :code:`(num_molecules, hidden_size)` containing the encoding of each molecule.
        """
        encodings = [enc(ba) for enc, ba in zip(self.encoder, batch)]
        output = reduce(lambda x, y: torch.cat((x, y), dim=1), encodings)
        return output
