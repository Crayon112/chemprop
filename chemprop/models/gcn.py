from torch.nn import Parameter
import torch.nn.functional as F

from functools import reduce

import torch
import torch.nn as nn

from chemprop.args import TrainArgs
from chemprop.features import BatchMolGraph, get_atom_fdim


class graphconvolution(nn.Module):

    def __init__(self, input_features, output_features):
        super(graphconvolution, self).__init__()
        self.input_features = input_features
        self.output_features = output_features
        self.weight = Parameter(
            torch.FloatTensor(input_features, output_features),
            requires_grad = True,
        )
        self.bias = Parameter(
            torch.FloatTensor(output_features),
            requires_grad = True,
        )
        self.fc = nn.Linear(input_features,output_features)

    def forward(self, feature, A):
        feature_w = F.relu(self.fc(feature))
        feature_a = torch.matmul(A, feature_w)
        return feature_a


class GCNEncoder(nn.Module):

    def __init__(self, args: TrainArgs, atom_fdim: int):
        super(GCNEncoder, self).__init__()
        self.hidden_dim = args.hidden_size
        self.device = args.device
        self.n_node = atom_fdim

        self.gc1 = graphconvolution(self.n_node, self.hidden_dim)
        self.gc2 = graphconvolution(self.hidden_dim, self.hidden_dim)
        self.gc3 = graphconvolution(self.hidden_dim, self.hidden_dim)

        self.aggregation = args.aggregation
        self.aggregation_norm = args.aggregation_norm

        # Cached zeros
        self.cached_zero_vector = nn.Parameter(torch.zeros(self.hidden_dim), requires_grad=False)

    def forward(self, mol_graph: BatchMolGraph, *args, **kwargs):
        feature = mol_graph.f_atoms.to(self.device)
        A = mol_graph.adj.to(self.device)
        feature_gc = self.gc1(feature, A)
        for i in range(5):
            feature_gc = self.gc2(feature_gc, A)
        feature_gc = self.gc3(feature_gc, A)

        # Readout
        mol_vecs = []
        for _, (a_start, a_size) in enumerate(mol_graph.a_scope):
            if a_size == 0:
                mol_vecs.append(self.cached_zero_vector)
            else:
                cur_hiddens = feature_gc.narrow(0, a_start, a_size)
                mol_vec = cur_hiddens  # (num_atoms, hidden_size)
                if self.aggregation == 'mean':
                    mol_vec = mol_vec.sum(dim=0) / a_size
                elif self.aggregation == 'sum':
                    mol_vec = mol_vec.sum(dim=0)
                elif self.aggregation == 'norm':
                    mol_vec = mol_vec.sum(dim=0) / self.aggregation_norm
                mol_vecs.append(mol_vec)

        mol_vecs = torch.stack(mol_vecs, dim=0)  # (num_molecules, hidden_size)

        return mol_vecs


class GCN(nn.Module):
    """An :class:`GCN` is a wrapper around :class:`GCNEncoder` which featurizes input as needed."""

    def __init__(self, args: TrainArgs, atom_fdim: int = None, **kwargs):
        """
        :param args: A :class:`~chemprop.args.TrainArgs` object containing model arguments.
        :param atom_fdim: Atom feature vector dimension.
        """
        super(GCN, self).__init__()
        self.atom_fdim = atom_fdim or get_atom_fdim()
        self.device = args.device

        if args.mpn_shared:
            self.encoder = nn.ModuleList(
                [GCNEncoder(args, self.atom_fdim)] * args.number_of_molecules
            )
        else:
            self.encoder = nn.ModuleList([
                GCNEncoder(args, self.atom_fdim)
                for _ in range(args.number_of_molecules)
            ])

    def forward(self, batch, *args, **kwargs) -> torch.FloatTensor:
        """
        Encodes a batch of molecules.

        :param batch: A list of list of SMILES, a list of list of RDKit molecules, or a
                      list of :class:`~chemprop.features.featurization.BatchMolGraph`.
                      The outer list or BatchMolGraph is of length :code:`num_molecules` (number of datapoints in batch),
                      the inner list is of length :code:`number_of_molecules` (number of molecules per datapoint).
        :return: A PyTorch tensor of shape :code:`(num_molecules, hidden_size)` containing the encoding of each molecule.
        """
        encodings = [enc(ba) for enc, ba in zip(self.encoder, batch)]
        output = reduce(lambda x, y: torch.cat((x, y), dim=1), encodings)
        return output
