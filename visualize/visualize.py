# !usr/bin/env python
# -*- encoding: utf-8 -*-
"""可视化函数.

@File: visualize.py
@Time: 2022/07/31 15:18:16
@Author: Crayon112
@SoftWare: VSCode
@Description: 可视化函数

"""
import os
import matplotlib.pyplot as plt
import numpy as np

from summary import dataset_summary, overall_auc


def dataset_pie(target, out_path=''):
    if not out_path:
        out_path = f'./visualize/pie/{target}-pie.png'
    s = dataset_summary(target)
    active, nonactive = s["active"], s["deactive"]
    plt.pie(
        [active, nonactive],
        explode=[0.01, 0.01],
        labels=["active", "nonactive"],
        autopct="%1.2f%%"
    )
    plt.title(s["protein"] + '-' + target)
    plt.savefig(out_path)
    plt.close()


def all_dataset_stack(outfile=''):
    if not outfile:
        outfile = './visualize/stack/stack.png'
    root, targets = '.', []

    from itertools import product
    for dataset, _, _ in product(
        ["DNMT1", "HAT", "HDAC", "HDM", "HMT"],
        ["GGNN", "GCN", "MPN"],
        ["Adam", "SGD"],
    ):
        read_dir = f'data/{dataset}'
        cur_drct = os.path.join(root, read_dir)
        for filename in os.listdir(cur_drct):
            target = filename.split('.')[0]
            targets.append(target)

    actives, deactives = [], []
    for target in targets:
        s = dataset_summary(target)
        actives.append(s["active"] / (s["active"] + s["deactive"]))
        deactives.append(s["deactive"] / (s["active"] + s["deactive"]))

    plt.figure(figsize=(12, 8))
    plt.bar(targets, actives, color='r')
    plt.bar(targets, deactives, bottom=actives, color='b')
    plt.xticks(rotation=270)
    plt.legend(["active", "nonactive"])
    plt.xlabel("Target")
    plt.savefig(outfile)
    plt.close()


def dataset_auc(dataset: str, platform: str = 'ubuntu') -> dict:
    """获取当前蛋白质下所有的模型表现."""
    drct = f'./result/{platform}/classification/{dataset}'
    summary = overall_auc(drct)
    summary = list(filter(
        lambda x: x.optimizer != 'SGD' and x.auc != 0, summary,
    ))

    targets_compare = dict()
    for s in summary:
        if s.target not in targets_compare:
            targets_compare[s.target] = dict()
        targets_compare[s.target][s.model] = s.auc
    return targets_compare

def plot_dateset_compare(dataset: str, outfile=''):
    """蛋白质之间的对比图."""
    if not outfile:
        outfile = f'./visualize/bar/{dataset}.png'
    performances = dataset_auc(dataset)

    targets = np.array([_ for _ in range(len(performances.keys()))])
    gcns = [x["GCN"] for x in performances.values()]
    ggnns = [x["GGNN"] for x in performances.values()]
    mpns = [x["MPN"] for x in performances.values()]

    _min, _max = min(gcns + ggnns + mpns), max(gcns + ggnns + mpns)

    width = 0.3

    plt.figure(figsize=(16, 8))
    plt.bar(targets - width, gcns, color='b', width=width, label='GCN')
    plt.bar(targets, ggnns, color='r', width=width, label='GGNN')
    plt.bar(targets + width, mpns, color='g', width=width, label='DMPNN')
    plt.xticks(targets, rotation=270, labels=[k for k in performances.keys()])
    plt.legend(["GCN", "GGNN", "DMPNN"])
    plt.xlabel("Targets")
    plt.ylabel("AUC")
    plt.ylim(_min - 0.01, _max + 0.01)
    plt.savefig(outfile)
    plt.close()


if __name__ == '__main__':
    # all_dataset_stack()
    for dataset in  ["DNMT1", "HAT", "HDAC", "HDM", "HMT"]:
        plot_dateset_compare(dataset)
