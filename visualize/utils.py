# !usr/bin/env python
# -*- encoding: utf-8 -*-
"""额外补充指标.

@File: utils.py
@Time: 2022/07/31 21:32:42
@Author: Crayon112
@SoftWare: VSCode
@Description: 额外补充指标

"""
import json


def mcc(tp, tn, fp, fn):
    if (tp + fp) * (tp + fn) * (tn + fp) * (tn + fn) == 0:
        return 0
    return (tp * tn - fp * fn) / (
        (tp + fp) * (tp + fn) * (tn + fp) * (tn + fn)
    ) ** 0.5


def accuracy(tp, tn, fp, fn):
    return (tp + tn) / (tp + tn + fp + fn)


def precision(tp, tn, fp, fn):
    if tp + fp == 0:
        return 0
    return tp / (tp + fp)


def recall(tp, tn, fp, fn):
    if tp + fn == 0:
        return 0
    return tp / (tp + fn)


def f1(tp, tn, fp, fn):
    pre, rec = precision(tp, tn, fp, fn), recall(tp, tn, fp, fn)
    if pre + rec == 0:
        return 0
    return 2 * pre * rec / (pre + rec)


if __name__ == '__main__':
    for data_f in [
        './visualize/summary-result-ubuntu.json',
        './visualize/summary-result-windows.json',
    ]:
        data = {}
        with open(data_f, 'r') as _data_f:
            data = json.load(_data_f)
            for d, s in data.items():
                data[d]["mcc"] = mcc(s['TP'], s['TN'], s['FP'], s['FN'])
                data[d]['f1'] = f1(s['TP'], s['TN'], s['FP'], s['FN'])
                data[d]['precision'] = precision(s['TP'], s['TN'], s['FP'], s['FN'])
                data[d]['recall'] = recall(s['TP'], s['TN'], s['FP'], s['FN'])
                data[d]['accuracy'] = accuracy(s['TP'], s['TN'], s['FP'], s['FN'])
        with open(data_f.replace('.json', '_.json'), 'w') as _data_f:
            json.dump(data, _data_f, indent=4)
