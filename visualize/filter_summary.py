# !usr/bin/env python
# -*- encoding: utf-8 -*-
"""过滤结果

@File: filter_summary.py
@Time: 2022/08/07 19:43:04
@Author: Crayon112
@SoftWare: VSCode
@Description: 过滤结果

"""

import json


def filter_data(data):
    _, data = data
    return data['optimizer'] == 'Adam' and data['mcc'] > 0.3 and data['model'] == 'MPN'


if __name__ == '__main__':
    data = {}
    with open('./visualize/summary-result-ubuntu_.json') as f:
        data = json.load(f)
        data = filter(filter_data, data.items())
        data = {k: v for k, v in data}
    print(data)
