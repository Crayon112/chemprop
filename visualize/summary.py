import csv
import json
import os
import re


class SingleProteinSummary(object):

    __slot__ = [
        "protein", "target", "model", "optimizer", "accurate",
        "active", "deactive"
    ]

    def __init__(
        self, protein, target, model, optimizer, auc,
        platform='ubuntu',
    ) -> None:
        self.protein = protein
        self.target = target
        self.model = model
        self.optimizer = optimizer
        self.auc = auc
        self.platform = platform

    def __repr__(self) -> str:
        return str({
            "protein": self.protein,
            "target": self.target,
            "model": self.model,
            "optimizer": self.optimizer,
            "auc": self.auc,
        })

    def to_json(self) -> dict:
        return {
            "protein": self.protein,
            "target": self.target,
            "model": self.model,
            "optimizer": self.optimizer,
            "auc": self.auc,
            "active": self.active,
            "deactive": self.deactive,
            'TP': self.TP, 'TN': self.TN,
            'FP': self.FP, 'FN': self.FN,
        }

    @property
    def active(self):
        with open(f'./data/{self.protein}/{self.target}.csv', 'r') as f:
            active, reader = 0, csv.reader(f)
            for row in reader:
                active += float(row[-1])
            return active

    @property
    def deactive(self):
        with open(f'./data/{self.protein}/{self.target}.csv', 'r') as f:
            reader = csv.reader(f)
            all_data_len = len([_ for _ in reader])
            return all_data_len - self.active

    def _to_label(self, label: str) -> int:
        try:
            if float(label) > 0.5:
                return 1
            return 0
        except Exception:
            return 0

    @property
    def labels(self):
        with open(f"./data/{self.protein}/{self.target}.csv", 'r') as f:
            labels = dict()
            for row in csv.reader(f):
                smile, label, *_ = row
                smile = self._process_to_smile(smile)
                if not self._is_smile(smile):
                    continue
                labels[smile] = self._to_label(label)
            return labels

    def _is_smile(self, smile: str) -> bool:
        return '=' in smile or 'C' in smile

    def _process_to_smile(self, smile: str) -> str:
        if smile.startswith('["') or smile.startswith("['"):
            smile = smile[2:-2]
        smile = smile.replace('\\\\', '\\')
        return smile

    @property
    def predicts(self):
        with open(
            f'./result/{self.platform}/classification/{self.protein}'
            f'/{self.target}/{self.model}/{self.optimizer}/1.0/test_preds.csv',
            'r',
        ) as f:
            labels = dict()
            for row in csv.reader(f):
                smile, label, *_ = row
                smile = self._process_to_smile(smile)
                if not self._is_smile(smile):
                    continue
                labels[smile] = self._to_label(label)
            return labels

    @property
    def TP(self):
        true_positive = 0
        for smile, predict in self.predicts.items():
            if self.labels[smile] == 1 and predict == 1:
                true_positive += 1
        return true_positive

    @property
    def FP(self):
        false_positive = 0
        for smile, predict in self.predicts.items():
            if self.labels[smile] == 0 and predict == 1:
                false_positive += 1
        return false_positive

    @property
    def FN(self):
        false_negative = 0
        for smile, predict in self.predicts.items():
            if self.labels[smile] == 1 and predict == 0:
                false_negative += 1
        return false_negative

    @property
    def TN(self):
        true_negative = 0
        for smile, predict in self.predicts.items():
            if self.labels[smile] == 0 and predict == 0:
                true_negative += 1
        return true_negative


def construct_ns_from_drct(drct: str, auc: float, platform='ubuntu') -> SingleProteinSummary:
    """从目录构建一个结构体，该结构体保留以下信息.

    1. 蛋白质名称；2. 靶点名称；3. 深度学习模型；
    4. 优化算法；5. AUC
    """
    drct = drct.split('classification/')[-1]
    protein, target, model, optimizer, *_ = drct.split(os.sep)
    this_summary = SingleProteinSummary(
        protein, target, model, optimizer, auc, platform=platform,
    )
    return this_summary

def _auc_from_log(log_path: str) -> float:
    """从日志文件中获取AUC.

    Parameters
    ==========
    log_path {str}: 日志路径

    Returns
    =======
    {float}: AUC值
    """
    with open(log_path, 'r') as log:
        log_src = log.read()
        auc = re.search(
            "Overall test auc = (?P<AUC>[0-9.]*) ",
            log_src, flags=re.M,
        )
        try:
            auc = auc.group('AUC')
            return float(auc)
        except Exception as e:  # noqa
            return 0.0


def overall_auc(drct: str) -> set:
    """获取文件夹下的所有准确率.

    Parameters
    ==========
    drct {str}: 根文件夹路径

    Returns
    =======
    {dict}: 文件夹下不同优化算法的准确率

    """
    summary = set()
    for rt, _, fns in os.walk(drct):
        for fn in fns:
            if fn == 'quiet.log':
                log_path = os.path.join(rt, fn)
                auc = _auc_from_log(log_path)
                platform = 'windows' if 'windows' in rt else 'ubuntu'
                summary.add(
                    construct_ns_from_drct(log_path, auc, platform=platform)
                )
    return summary


def dataset_summary(
    target: str, platform: str = 'ubuntu',
) -> dict:
    """提供数据集概览.

    包括激活态个数以及非激活态个数
    """
    cache_file = f'./visualize/summary-result-{platform}.json'
    if not os.path.isfile(cache_file):
        summary = generate_cache()
    else:
        with open(cache_file, 'r') as cache_f:
            summary = json.load(cache_f)
    for _, s in summary.items():
        if s["target"] == target:
            return s
    return {}


def generate_cache(platform: str = 'ubuntu') -> dict:
    """产生本地缓存文件记录结果."""
    # for root in ['./result/ubuntu', './result/windows']:
    root = f'./result/{platform}'
    summary = overall_auc(root)
    summary = sorted(
        summary, reverse=True,
        key=lambda x: (x.target, x.model, x.optimizer, x.auc),
    )
    summary = {f"data-{id(data)}": data.to_json() for data in summary}
    with open(
        f'./visualize/summary-{root.strip("./").replace(os.sep, "-")}.json',
        'w'
    ) as out:
        json.dump(summary, out, indent=4)
    return summary


if __name__ == '__main__':
    generate_cache()
