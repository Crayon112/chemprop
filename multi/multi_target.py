# !usr/bin/env python
# -*- encoding: utf-8 -*-
"""多分类任务

@File: multi_target.py
@Time: 2022/08/07 15:59:51
@Author: Crayon112
@SoftWare: VSCode
@Description: 多分类任务

"""
import sys
sys.path.extend(['.'])

import os
from chemprop.data.data import MoleculeDataLoader, MoleculeDatapoint, MoleculeDataset
from chemprop.data.utils import filter_invalid_smiles, get_data
from chemprop.train.predict import predict
from chemprop.utils import load_checkpoint
import numpy as np
import json


def construct_dataset(dataset_path, protein):
    with open(dataset_path, 'r') as f:
        data = json.load(f)
    smiles, targets = [], []
    for smile, d in data.items():
        smiles.append(smile)
        targets.append(d[protein])
    data = MoleculeDataset([
        MoleculeDatapoint(smile, target)
        for smile, target in zip(smiles, targets)
    ])
    return filter_invalid_smiles(data)


def predict_with_model(sample_path: str, model_path: str) -> np.ndarray:
    dataset = get_data(sample_path)
    # scaler = dataset.normalize_targets()
    samples = MoleculeDataLoader(dataset)
    model = load_checkpoint(model_path)
    preds = predict(model, samples)
    preds = [[float(pred[0] >= 0.5)] for pred in preds]
    return samples, preds


if __name__ == '__main__':
    result, root = {}, './mpn_models'
    for model in os.listdir(root):
        if not model.endswith('.pt'):
            continue
        model = os.path.join(root, model)
        protein = model.split(os.sep)[-1].split('.')[0]
        samples, preds = predict_with_model('./multi_dataset/filter_dataset.csv', model)
        result[protein] = {smile[0]: pred[0] for smile, pred in zip(
            samples.dataset.smiles(), preds
        )}
    with open('filter_mpn.json', 'w') as f:
        json.dump(result, f, indent=4)
