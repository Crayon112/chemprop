# !usr/bin/env python
# -*- encoding: utf-8 -*-
"""多分类指标

@File: metric.py
@Time: 2022/08/14 12:44:53
@Author: Crayon112
@SoftWare: VSCode
@Description: 多分类指标

"""

from sklearn.metrics import multilabel_confusion_matrix
import pandas
import numpy


def groud_truth(src_path):
    data = pandas.read_csv(src_path).to_numpy()
    data = data[:, 1:].astype(int)
    return data


def predict(pred_path):
    data = pandas.read_csv(pred_path).to_numpy()
    data = data[:, 1:].T.astype(int)
    return data


if __name__ == '__main__':
    y_true = groud_truth('./result/dataset.csv')
    y_pred = predict('./result/ggnn.csv')
    matrix = multilabel_confusion_matrix(y_true, y_pred)

    confusion_matrix = numpy.array([0, 0, 0, 0]).T.reshape(-1, 2, 2)
    for confusion in matrix:
        confusion_matrix += confusion

    TN, FP, FN, TP = confusion_matrix.ravel()

    TPR = TP/(TP+FN)
    # Specificity or true negative rate
    TNR = TN/(TN+FP)
    # Precision or positive predictive value
    PPV = TP/(TP+FP)
    # Negative predictive value
    NPV = TN/(TN+FN)
    # Fall out or false positive rate
    FPR = FP/(FP+TN)
    # False negative rate
    FNR = FN/(TP+FN)
    # False discovery rate
    FDR = FP/(TP+FP)
    # Overall accuracy
    ACC = (TP+TN)/(TP+FP+FN+TN)

    print(locals())
