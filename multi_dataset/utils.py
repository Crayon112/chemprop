# !usr/bin/env python
# -*- encoding: utf-8 -*-
"""辅助函数

@File: utils.py
@Time: 2022/08/07 15:39:19
@Author: Crayon112
@SoftWare: VSCode
@Description: 辅助函数

"""
import os
import pandas as pd
import json

def dataset_to_json(drct):
    for root, _, filenames in os.walk(drct):
        for filename in filenames:
            if filename.endswith('csv'):
                filepath = os.path.join(root, filename)
                dataframe = pd.read_csv(filepath, header=None)
                json_path = os.path.join(root, filename.replace('csv', "json"))
                with open(json_path, 'w') as f:
                    json_data = dataframe.to_json(orient='records')
                    json_data = json.loads(json_data)
                    for idx, data in enumerate(json_data):
                        json_data[idx] = {data["0"]: data["1"]}
                    json.dump(json_data, f, indent=4)


def csv_to_json(filepath):
    protein = filepath.split(os.sep)[-1].split('.')[0]
    dataframe = pd.read_csv(filepath, header=None)
    json_data = dataframe.to_json(orient='records')
    json_data = json.loads(json_data)
    for idx, data in enumerate(json_data):
        json_data[idx] = {data["0"]: {protein: data["1"]}}
    return json_data


def multi_dataset(drct):
    dataset, proteins = {}, []
    for root, _, filenames in os.walk(drct):
        for filename in filenames:
            json_data = []
            if filename.endswith('csv'):
                filepath = os.path.join(root, filename)
                json_data.extend(csv_to_json(filepath))
            protein = filepath.split(os.sep)[-1].split('.')[0]
            proteins.append(protein)
            for data in json_data:
                for smile, data in data.items():
                    if smile not in dataset:
                        dataset[smile] = {}
                    dataset[smile].update(data)
    for smile, data in dataset.items():
        for protein in proteins:
            if protein not in data:
                dataset[smile].update({protein: 0})
    return dataset


def filter_dataset(data, valve = 4):
    s = 0
    data = data[-1]
    for k, v in data.items():
        if v == 1:
            s += 1
    return s >= valve


if __name__ == '__main__':
    dataset = multi_dataset('./data')
    with open('dataset.json', 'w') as f:
        json.dump(dataset, f, indent=4)
    dataset = list(filter(filter_dataset, dataset.items()))
    dataset = {k: v for k, v in dataset}
    with open('filter_dataset.json', 'w') as f:
        json.dump(dataset, f, indent=4)
