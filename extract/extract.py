import os


def save_protein_model(protein, inpath, model='MPN'):
    os.system(f'cp {inpath} ./{model.lower()}_models/{protein}.pt')

def save_args(protein, inpath, model='MPN'):
    os.system(f'cp {inpath} ./{model.lower()}_models/{protein}.json')


def extract_models(drct, model='MPN'):
    """从文件夹中抽取出最终模型.

    默认情况下使用 ubuntu 训练的模型最终叠作为最终模型；
    每个文件夹下中使用 MPN 对应的 Adam 优化器作为最终模型。

    """
    for root, _, filenames in os.walk(drct):
        for filename in filenames:
            if filename != "model.pt":
                continue
            filepath = os.path.join(root, filename)
            if (
                'Adam' in filepath and
                model in filepath and
                'fold_2' in filepath
            ):
                *_, protein, _, _, _, _, _, _ = filepath.split(os.sep)
                save_protein_model(protein, filepath, model)


def extract_model_args(drct, model='MPN'):
    """从文件夹中抽取出最终模型的参数.

    默认情况下使用 ubuntu 训练的模型最终叠作为最终模型；
    每个文件夹下中使用 MPN 对应的 Adam 优化器作为最终模型。

    """
    for root, _, filenames in os.walk(drct):
        for filename in filenames:
            if filename != "args.json":
                continue
            filepath = os.path.join(root, filename)
            if (
                'Adam' in filepath and
                model in filepath and
                'fold_2' in filepath
            ):
                *_, protein, _, _, _, _, _ = filepath.split(os.sep)
                save_args(protein, filepath, model)
